
npm i Установить зависимости
npm start Запустить сборку, сервер и слежение за файлами
npm start ЗАДАЧА Запустить задачу с названием ЗАДАЧА список задач в gulpfile.js
npm run build Сборка проекта без карт кода (сжатый вид, как результат работы)
npm run lint:scss Проверка стилей проекта https://stylelint.io/
npm run lint:scss Проверка js https://eslint.org/

1. [autoprefixer](https://github.com/postcss/autoprefixer)
2. [css-mqpacker](https://github.com/hail2u/node-css-mqpacker)
3. [postcss-import](https://github.com/postcss/postcss-import)
4. [postcss-inline-svg](https://github.com/TrySound/postcss-inline-svg)
5. [gulp-cleancss](https://github.com/mgcrea/gulp-cleancss) (только в режиме сборки без карт кода)
6. [postcss-object-fit-images](https://github.com/ronik-design/postcss-object-fit-images) (в паре с [полифилом](https://github.com/bfred-it/object-fit-images))

## Назначение папок

```bash
  build/            # Папка сборки, здесь работает сервер автообновлений.
  src/              # Исходные файлы
  pages/            # - html страницы проекта
  _include/         # - фрагменты html для вставки на страницы
  style/            # - стили scss
  style/partials/   # - фрагменты scss
  style/vendor/     # - стили подключенных библиотек
  fonts/            # - шрифты проекта
  media/            # - медиа файлы проекта
  images/           # - изображения
  images/img_to_bg/ # - svg для вставки inline в стили (не будут скопированы в build)
  js/               # - js-файлы
  js/partials/      # - модули js
  js/vendor/        # - подключенный библиотеки js
  index.html        # - главная страница проекта
